# Tipeacript-template

Simple template of a typescript project in MVC with 2 apis as example and the tests

## Run App

With script

```
sh dev-init.sh
```

OR

Step by step

1. Go to the api folder of the app

```
cd atix-challenge/api
```

2. Install dependencies

```
npm i
```

3. Start api

```
npm run start-dev
```

## Run test

1. Go to the api folder of the app

```
cd typescript-template/api
```

2. Run Tests api

```
npm test
```

## Apis examples

- Adds a new line in the log

```
GET  localhost:5000/log/get
{
	"response": {
		"data": "Message response"
	}
}
```

## Notes

This tempalte is made in typescript and were used the followings libraries:

- express https://www.npmjs.com/package/express
- @types/node https://www.npmjs.com/package/@types/node
- @types/express https://www.npmjs.com/package/@types/express
- jest https://www.npmjs.com/package/jest
