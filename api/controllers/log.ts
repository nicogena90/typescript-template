import { Response } from "express";
import * as httpResp from "./httpResponses";

const FILE_LOG = "controllers/log.ts";

/**
 * Example of a post
 *
 * @param req   Request
 * @param res   Response
 */
export async function add(req: any, res: Response) {
  try {
    const { message } = req.body;

    if (!message) {
      httpResp.error(
        res,
        app.httpResponses.errorCode.BAD_REQUEST,
        `The message field is mandatory`
      );
    }

    httpResp.success(res, `Request received successfully`);
  } catch (error) {
    httpResp.error(
      res,
      app.httpResponses.errorCode.INTERNAL_SERVER_ERROR,
      `${FILE_LOG} (add): error (${error.stack})`
    );
  }
}

/**
 * Example of a get
 *
 * @param req   Request
 * @param res   Response
 */
export async function get(req: any, res: Response) {
  try {
    httpResp.success(res, `Message response`);
  } catch (error) {
    httpResp.error(
      res,
      app.httpResponses.errorCode.INTERNAL_SERVER_ERROR,
      `${FILE_LOG} (add): error (${error.stack})`
    );
  }
}
