import * as express from "express";
const app = express();

import logRoutes from "./routes/log";

app.use(express.json({ limit: "50mb" }));

// Load the routes
app.use("/log/", logRoutes);

export default app;
