import * as request from "supertest";
import app from "../app";

const port = 5000;
let server: any;

beforeAll(() => {
  server = app.listen(port);
});

afterAll(() => {
  server.close();
});

describe("test get products", () => {
  it("can get products", async () => {
    await request(server)
      .get("/log")
      .expect(200)
      .expect({ response: { data: "Message response" } });
  });
});
