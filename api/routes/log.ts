import * as logController from "../controllers/log";

var express = require("express");
var api = express("log");

api.get("", logController.get);
api.post("", logController.add);

export default api;
